﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusclePart : BodyPart
{
    public Gene strength;
    public Gene speedMax;
    public Gene speedFrequency;
    public Gene speedPhaseOffset;
    public Gene axisTwist;
    public Gene angleMax;
    public Gene angleMin;
    private ConnectionPoint parentConnectionPoint;
    private float lifeTime;

    public override void OnSpawned()
    {
        base.OnSpawned();

        var joint = gameObject.AddComponent<HingeJoint>();

        transform.rotation *= Quaternion.FromToRotation(Vector3.up, -parentConnectionPoint.transform.up);
        transform.position += parentConnectionPoint.transform.position - transform.TransformPoint(joint.anchor);

        joint.connectedBody = parentConnectionPoint.GetComponentInParent<Rigidbody>();

        joint.axis = joint.axis + Vector3.forward * axisTwist.Value;

        var newMotor = joint.motor;
        newMotor.force = strength.Value;
        newMotor.targetVelocity = 0;
        joint.motor = newMotor;
        joint.useMotor = true;

        var newLimits = joint.limits;
        newLimits.min = angleMin.Value;
        newLimits.max = angleMax.Value;
        joint.limits = newLimits;
        joint.useLimits = true;
    }

    public override void GetGenes(List<Gene> allGenes)
    {
        base.GetGenes(allGenes);

        allGenes.Add(strength);
        allGenes.Add(speedMax);
        allGenes.Add(speedFrequency);
        allGenes.Add(speedPhaseOffset);
        allGenes.Add(axisTwist);
        allGenes.Add(angleMax);
        allGenes.Add(angleMin);
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        var joint = GetComponent<HingeJoint>();
        if (!joint)
            return;

        lifeTime += Time.fixedDeltaTime;

        var newMotor = joint.motor;
        newMotor.targetVelocity = Mathf.Lerp(-speedMax.Value, speedMax.Value, 0.5f + 0.5f * Mathf.Sin((speedPhaseOffset.Value + lifeTime * speedFrequency.Value) * 2 * Mathf.PI));
        joint.motor = newMotor;
    }

    public void SetJointConnectionPoint(ConnectionPoint parentConnectionPoint)
    {
        this.parentConnectionPoint = parentConnectionPoint;
    }
}
