﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalOrb : MonoBehaviour
{
    public Transform[] wobbleShells;
    float[] wobble;
    public bool taken { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        wobble = new float[wobbleShells.Length];
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        for (int i = 0; i < wobbleShells.Length; i++)
        {
            wobble[i] += (Random.value + i * 0.2f) * Time.fixedDeltaTime;
            float radius = i * 0.2f + 0.65f;
            wobbleShells[i].localScale = new Vector3(
                radius + 0.1f * Mathf.Sin(wobble[i] * 7), 
                radius + 0.1f * Mathf.Sin(wobble[i] * 9), 
                radius + 0.1f * Mathf.Sin(wobble[i] * 13));
        }

        if (taken)
        {
            foreach (var renderer in GetComponentsInChildren<MeshRenderer>())
            {
                var newMat = renderer.material;
                var newCol = newMat.color;
                newCol.a *= 0.982f;
                newMat.color = newCol;
                renderer.material = newMat;
                renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            }
            transform.localScale *= 1.01f;
        }
        else
        {
            foreach(var renderer in GetComponentsInChildren<MeshRenderer>())
            {
                renderer.enabled = Spawner.canSelect;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!Spawner.canSelect)
            return;

        if(other.GetComponentInParent<Creature>())
        {
            SetAsTaken();
        }
    }

    void SetAsTaken()
    {
        if (taken)
            return;

        taken = true;
        GetComponent<ParticleSystem>().Play();
        GetComponent<AudioSource>().Play();
        GetComponent<Collider>().enabled = false;

        foreach (var renderer in GetComponentsInChildren<MeshRenderer>())
        {
            renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        }

        Destroy(gameObject, 5.0f);
    }
}
