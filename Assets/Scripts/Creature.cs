﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature : MonoBehaviour
{
    public bool randomizeOnStart = false;

    List<Gene> genes = new List<Gene>();
    public BodyPart mainBody;

    private Vector3 startPos;
    private Vector3 prevPos;
    private float distanceMoved = 0;

    public bool selectedToBreed = false;

    public void Start()
    {
        if (randomizeOnStart)
        {
            InitGenesRandomly();
        }
    }

    public void Init(BodyPart mainBody, Vector3 startPos)
    {
        this.mainBody = mainBody;
        this.startPos = startPos;
    }

    public void InitGenesRandomly()
    {
        genes.Clear();

        var geneCollections = GetComponentsInChildren<IGeneCollection>();
        foreach (var geneCollection in geneCollections)
        {
            geneCollection.GetGenes(genes);
        }

        foreach(var gene in genes)
        {
            gene.InitNewRandomized();
        }

        foreach(var geneCollection in geneCollections)
        {
            geneCollection.OnSpawned();
        }

    }

    public void InitGenesAfterBreeding()
    {
        genes.Clear();

        var geneCollections = GetComponentsInChildren<IGeneCollection>();
        foreach (var geneCollection in geneCollections)
        {
            geneCollection.GetGenes(genes);
        }

        foreach (var geneCollection in geneCollections)
        {
            geneCollection.OnSpawned();
        }
    }

    public float Goodness()
    {
        if (mainBody == null)
            return -100;
        Vector3 moved = mainBody.transform.position - startPos;
        moved.y = 0;
        return moved.magnitude + 0.2f * distanceMoved;
        //return mainBody.transform.position.z - startPos.z;// + distanceMoved * 0.1f;
    }

    private void FixedUpdate()
    {
        if(mainBody == null)
        {
            // died
            Destroy(gameObject);
            return;
        }

        Vector3 moved = mainBody.transform.position - prevPos;
        moved.y = 0;

        distanceMoved += moved.magnitude;

        prevPos = mainBody.transform.position;
    }

    internal void ToggleSelectedToBreed()
    {
        selectedToBreed = !selectedToBreed;
        foreach (var bodypart in GetComponentsInChildren<BodyPart>())
            bodypart.RenderAsSelected(selectedToBreed);
    }

    internal void Kill()
    {
        mainBody?.Kill(true);
        Destroy(gameObject);
    }
}
