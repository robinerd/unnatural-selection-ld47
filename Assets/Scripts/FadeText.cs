﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeText : MonoBehaviour
{
    Color originalColor;
    Color col;

    const float tick = 0.05f;
    // Start is called before the first frame update
    void Start()
    {
        originalColor = GetComponent<Text>().color;
        col = originalColor;
        col.a = 0;
        GetComponent<Text>().color = col;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal void ShowHide(float duration)
    {
        StartCoroutine(ShowHide_Coroutine(duration));
    }

    private IEnumerator ShowHide_Coroutine(float duration)
    {
        yield return StartCoroutine(Show_Coroutine(duration * 0.2f));
        yield return new WaitForSeconds(duration * 0.4f);
        yield return StartCoroutine(Hide_Coroutine(duration * 0.4f));
    }

    private IEnumerator Show_Coroutine(float duration)
    {
        float timer = duration;
        while(timer > 0)
        {
            col.a += originalColor.a * tick / duration;
            GetComponent<Text>().color = col;
            if (col.a >= originalColor.a)
                break;
            timer -= tick;
            yield return new WaitForSeconds(tick);
        }
    }
    private IEnumerator Hide_Coroutine(float duration)
    {
        float timer = duration;
        while (timer > 0)
        {
            col.a -= originalColor.a * tick / duration;
            GetComponent<Text>().color = col;
            if (col.a <= 0)
                break;
            timer -= tick;
            yield return new WaitForSeconds(tick);
        }
    }

    public void Hide(float duration)
    {
        StartCoroutine(Hide_Coroutine(duration));
    }
    public void Show(float duration)
    {
        StartCoroutine(Show_Coroutine(duration));
    }
}
