﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BodyPart : MonoBehaviour, IGeneCollection
{
    public Material selectionMaterial;
    public GameObject deathEffectPrefab;
    public GameObject selectEffectPrefab;
    public GameObject deselectEffectPrefab;

    public Gene scaleX;
    public Gene scaleY;
    public Gene scaleZ;
    public Gene rotX;
    public Gene rotY;
    public Gene rotZ;

    private Material originalMaterial;

    public virtual void GetGenes(List<Gene> allGenes)
    {
        allGenes.Add(scaleX);
        allGenes.Add(scaleY);
        allGenes.Add(scaleZ);
        allGenes.Add(rotX);
        allGenes.Add(rotY);
        allGenes.Add(rotZ);
    }

    public virtual void OnSpawned()
    {
        UpdateScale();
        originalMaterial = GetComponent<MeshRenderer>().sharedMaterial;
    }

    void UpdateScale()
    {
        transform.localScale = new Vector3(scaleX.Value, scaleY.Value, scaleZ.Value);
        transform.localRotation = Quaternion.Euler(rotX.Value, rotY.Value, rotZ.Value);

        var body = GetComponent<Rigidbody>();
        body.mass = scaleX.Value * scaleY.Value * scaleZ.Value;
    }

    public List<Gene> GetGenes()
    {
        var genes = new List<Gene>();
        GetGenes(genes);
        return genes;
    }

    public void Kill(bool playAudio = true)
    {
        foreach(var connection in GetComponentsInChildren<ConnectionPoint>())
        {
            if (connection.attachedPart)
                connection.attachedPart.Kill(false);
        }

        var deathEffect = Instantiate(deathEffectPrefab, transform.position, deathEffectPrefab.transform.rotation);

        if (playAudio)
        {
            var deathAudio = deathEffect.GetComponent<RandomSound>();
            deathAudio.Play();
        }
        Destroy(deathEffect, 3.0f);
        Destroy(gameObject);
    }

    protected virtual void FixedUpdate()
    {
        if (transform.position.y < -4.5) // Below ground. Hard coded for now!
            Kill();
    }

    private void OnMouseDown()
    {
        if (!Spawner.canSelect)
            return;

        var creature = GetComponentInParent<Creature>();
        creature.ToggleSelectedToBreed();
        GameObject effect;
        if (creature.selectedToBreed)
            effect = Instantiate(selectEffectPrefab, transform.position, Quaternion.identity);
        else
            effect = Instantiate(deselectEffectPrefab, transform.position, Quaternion.identity);
        Destroy(effect, 4.0f);
    }

    internal void RenderAsSelected(bool selectedToBreed)
    {
        if (!Spawner.canSelect)
            return;

        if (selectedToBreed)
            GetComponent<MeshRenderer>().sharedMaterial = selectionMaterial;
        else
            GetComponent<MeshRenderer>().sharedMaterial = originalMaterial;
    }
}
