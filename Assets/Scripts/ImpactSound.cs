﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactSound : MonoBehaviour
{
    public GameObject effectPrefab;

    private void OnCollisionEnter(Collision collision)
    {
        if (!Spawner.canSelect)
            return;

        float impact = collision.relativeVelocity.magnitude;
        if (impact > 1.2f)
        {
            var effect = Instantiate(effectPrefab, collision.contacts[0].point, Quaternion.identity);
            effect.GetComponent<AudioSource>().volume *= Mathf.Min(impact / 5, 1.0f);
            effect.GetComponent<RandomSound>().Play();
            Destroy(effect, 1.0f);
        }
    }
}
