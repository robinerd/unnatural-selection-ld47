﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using System.Linq;
using JetBrains.Annotations;

public class Spawner : MonoBehaviour
{
    public int initialSpawnCount = 10;
    public BodyPart mainBodyPrefab;
    public MusclePart muscleBodyPrefab;
    public int hiddenGenerations = 3;
    [Min(0)]
    public float timeScale = 2.0f;
    public float timePerGeneration = 15;
    public FadeText selectParentsFirstText;
    public FadeText breedingText;
    public FadeText theEndText;

    public static bool canSelect = true;

    List<Creature> newGeneration = new List<Creature>();
    List<Creature> fittest = new List<Creature>();
    private bool generationInProgress;
    private GoalOrb[] goalOrbs;
    bool theEnd = false;

    public struct BreedingMuscle
    {
        public ConnectionPoint childConnection;
        public ConnectionPoint fatherConnection;
        public ConnectionPoint motherConnection;
    }

    // Start is called before the first frame update
    void Start()
    {
        canSelect = true;

        goalOrbs = FindObjectsOfType<GoalOrb>();
        breedingText.GetComponent<AudioSource>().pitch = 3.4f;
        SpawnFreshGeneration();
    }

    public void Update()
    {
        if (!generationInProgress)
        {
            if (Input.GetKeyDown(KeyCode.Return))
                BreedGeneration();
            else if (Input.GetKeyDown(KeyCode.R))
                SpawnFreshGeneration();
        }

        if(!theEnd)
        {
            theEnd = !goalOrbs.Any(orb => !orb.taken);
            if(theEnd)
            {
                canSelect = false;
                theEndText.Show(5.0f);
                theEndText.GetComponent<AudioSource>().Play();
            }
        }
    }

    public void BreedGeneration()
    {
        if (generationInProgress || theEnd)
            return;
        StartCoroutine(BreedGeneration_Coroutine());
    }

    public IEnumerator BreedGeneration_Coroutine()
    {
        fittest.Clear();
        //fittest.AddRange(newGeneration.Where(creature => creature != null).OrderByDescending(creature => creature.Goodness()).Take(2));
        fittest.AddRange(newGeneration.Where(creature => creature != null && creature.selectedToBreed));
        if (fittest.Count == 0)
        {
            selectParentsFirstText.ShowHide(4f);
            yield break;
        }

        generationInProgress = true;
        canSelect = false;
        breedingText.Show(5);
        breedingText.GetComponent<AudioSource>().volume = 0.12f;
        foreach (var creature in newGeneration.Where(creature => creature != null))
        {
            if (!fittest.Contains(creature))
                creature.Kill();
        }

        yield return new WaitForSeconds(2.5f);

        // Hidden generations
        Time.timeScale = 9;
        for (int i = 0; i < hiddenGenerations; i++)
        {
            SpawnNextGeneration(50);

            foreach (var oldWinner in fittest)
            {
                Destroy(oldWinner.gameObject);
            }
            yield return new WaitForSeconds(timePerGeneration);

            fittest.Clear();
            fittest.AddRange(newGeneration.Where(creature => creature != null).OrderByDescending(creature => creature.Goodness()).Take(5));

            foreach (var creature in newGeneration.Where(creature => creature != null))
            {
                if (!fittest.Contains(creature))
                    Destroy(creature.gameObject);
            }
        }
        Time.timeScale = timeScale;
        breedingText.Hide(2);
        breedingText.GetComponent<AudioSource>().volume = 0.0f;

        //The real generation

        //yield return new WaitForSeconds(2.0f);

        SpawnNextGeneration(initialSpawnCount);

        foreach(var oldWinner in fittest.Where(creature => creature != null))
        {
            Destroy(oldWinner.gameObject);
        }

        canSelect = true;
        generationInProgress = false;
    }

    public void SpawnFreshGeneration()
    {
        if (generationInProgress || theEnd)
            return;

        foreach (var creature in newGeneration.Where(creature => creature != null))
        {
            creature.Kill();
        }

        newGeneration.Clear();
        for (int i = 0; i < 20; i++)
        {
            var newCreature = SpawnNew(Vector3.up * 1000); // Spawn out of view
            newGeneration.Add(newCreature);
        }

        // Create new generations just to get more longer limbs, don't care about fitness yet.
        for (int i = 0; i < 2; i++)
        {
            fittest.Clear();
            fittest.AddRange(newGeneration.Where(creature => creature != null));

            foreach (var creature in newGeneration.Where(creature => creature != null))
            {
                if (!fittest.Contains(creature))
                    Destroy(creature.gameObject);
            }
            SpawnNextGeneration(initialSpawnCount);
            foreach (var oldWinner in fittest.Where(creature => creature != null))
            {
                Destroy(oldWinner.gameObject);
            }
        }
    }

    public void SpawnNextGeneration(int spawnCount)
    {
        newGeneration.Clear();
        for (int i = 0; i < spawnCount; i++)
        {
            Vector3 radialOffset = Quaternion.Euler(0, 360.0f * i / spawnCount, 0) * Vector3.forward * 8;
            var newCreature = SpawnOffspring(radialOffset);
            if(newCreature != null && newCreature.mainBody != null)
                newGeneration.Add(newCreature);
        }
    }

    Creature SpawnNew(Vector3 offset)
    {
        var creatureObject = new GameObject("Creature");
        creatureObject.transform.position = transform.position + offset;

        var creature = creatureObject.AddComponent<Creature>();
        var mainbody = Instantiate(mainBodyPrefab, creature.transform.position, Quaternion.identity, creature.transform);
        foreach (var connection in mainbody.GetComponentsInChildren<ConnectionPoint>())
        {
            if (Random.value < 0.5f)
                continue;

            var musclePart = Instantiate(muscleBodyPrefab, mainbody.transform.position, Quaternion.identity, creature.transform);
            musclePart.SetJointConnectionPoint(connection);
            connection.attachedPart = musclePart;
        }

        creature.Init(mainbody, mainbody.transform.position);
        //creature.Init(mainbody, transform.position);
        creature.InitGenesRandomly();

        return creature;
    }

    Creature SpawnOffspring(Vector3 offset)
    {
        var creatureObject = new GameObject("Creature");
        creatureObject.transform.position = transform.position + offset;

        var mother = fittest[Random.Range(0, fittest.Count)];
        var father = fittest[Random.Range(0, fittest.Count)];

        var creature = creatureObject.AddComponent<Creature>();
        var mainbody = Instantiate(mainBodyPrefab, creature.transform.position, Quaternion.identity, creature.transform);
        BreedGeneCollection(mainbody, mother.mainBody, father.mainBody);

        var musclesToSpawn = new List<BreedingMuscle>();
        musclesToSpawn.AddRange(ExtractMusclesToSpawn(mainbody, mother.mainBody, father.mainBody));

        int iterationCount = 0;
        while (musclesToSpawn.Count > 0)
        {
            iterationCount++;
            if(iterationCount > 20)
            {
                Debug.LogWarning("Max iterations reached when spawning offspring. Force-exit loop.");
                break;
            }

            var nextLevelOfMuscles = new List<BreedingMuscle>();
            foreach (var muscleSpawner in musclesToSpawn)
            {
                var childConnection = muscleSpawner.childConnection;
                var motherMuscle = muscleSpawner.motherConnection?.attachedPart;
                var fatherMuscle = muscleSpawner.fatherConnection?.attachedPart;

                if (motherMuscle && fatherMuscle)
                {
                    if (Random.value > 0.07f) // Chance to remove part that both parents have
                    {
                        var childMuscle = Instantiate(muscleBodyPrefab, mainbody.transform.position, Quaternion.identity, creature.transform);
                        childMuscle.SetJointConnectionPoint(childConnection);
                        childConnection.attachedPart = childMuscle;
                        BreedGeneCollection(childMuscle, motherMuscle, fatherMuscle);

                        nextLevelOfMuscles.AddRange(ExtractMusclesToSpawn(childMuscle, motherMuscle, fatherMuscle));
                    }
                }
                else if (!motherMuscle && !fatherMuscle)
                {
                    if (Random.value < 0.03f)
                    {
                        // Add body part even if no parent has it
                        var childMuscle = Instantiate(muscleBodyPrefab, mainbody.transform.position, Quaternion.identity, creature.transform);
                        childMuscle.SetJointConnectionPoint(muscleSpawner.childConnection);
                        childConnection.attachedPart = childMuscle;
                        foreach (var gene in childMuscle.GetGenes())
                        {
                            gene.InitNewRandomized();
                        }

                        // Don't add to "nextLevelOfMuscles" since this is already a new muscle. That's enough for this generation!
                    }
                }
                else
                {
                    // Only one of the parents has a connected part
                    if (Random.value < 0.4f)
                    {
                        var childMuscle = Instantiate(muscleBodyPrefab, mainbody.transform.position, Quaternion.identity, creature.transform);
                        childMuscle.SetJointConnectionPoint(childConnection);
                        childConnection.attachedPart = childMuscle;
                        var parentMuscle = motherMuscle ? motherMuscle : fatherMuscle; // Will never be null
                        var muscleGenes = childMuscle.GetGenes();
                        var parentMuscleGenes = parentMuscle.GetGenes();
                        if (muscleGenes.Count == parentMuscleGenes.Count)
                        {
                            for (int j = 0; j < muscleGenes.Count; j++)
                            {
                                muscleGenes[j].InitFromSoloParent(parentMuscleGenes[j]);
                            }

                            nextLevelOfMuscles.AddRange(ExtractMusclesToSpawn(childMuscle, motherMuscle, fatherMuscle));
                        }
                        else
                            Debug.LogError("Not same number of genes in parent muscle and child muscle", childMuscle);
                    }
                }
            }

            musclesToSpawn = nextLevelOfMuscles;
        }

        creature.Init(mainbody, mainbody.transform.position);
        //creature.Init(mainbody, transform.position);
        creature.InitGenesAfterBreeding();

        return creature;
    }

    private IEnumerable<BreedingMuscle> ExtractMusclesToSpawn(BodyPart child, BodyPart mother, BodyPart father)
    {
        var childConnections = child.GetComponentsInChildren<ConnectionPoint>();
        var motherConnections = mother ? mother.GetComponentsInChildren<ConnectionPoint>() : null;
        var fatherConnections = father ? father.GetComponentsInChildren<ConnectionPoint>() : null;
        for(int i = 0; i < childConnections.Length; i++)
        {
            yield return new BreedingMuscle()
            {
                childConnection = childConnections[i],
                motherConnection = motherConnections != null ? motherConnections[i] : null,
                fatherConnection = fatherConnections != null ? fatherConnections[i] : null
            };
        }
    }

    void BreedGeneCollection(IGeneCollection child, IGeneCollection mother, IGeneCollection father)
    {
        var motherGenes = new List<Gene>();
        mother.GetGenes(motherGenes);
        var fatherGenes = new List<Gene>();
        father.GetGenes(fatherGenes);
        if(motherGenes.Count != fatherGenes.Count)
        {
            Debug.LogError("Different gene count in parents");
            return;
        }

        var childGenes = new List<Gene>();
        child.GetGenes(childGenes);
        if (childGenes.Count != fatherGenes.Count)
        {
            Debug.LogError("Child gene count not same as parents");
            return;
        }

        for (int i = 0; i < childGenes.Count; i++)
            childGenes[i].InitFromParents(motherGenes[i], fatherGenes[i]);
    }
}
