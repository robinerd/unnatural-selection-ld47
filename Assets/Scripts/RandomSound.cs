﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSound : MonoBehaviour
{
    [Range(0, 0.5f)]
    public float randomizePitch = 0.2f;
    public AudioClip[] clips;

    public bool PlayOnAwake = false;

    // Start is called before the first frame update
    void Start()
    {
        if (PlayOnAwake)
            Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Play()
    {
        var audio = GetComponent<AudioSource>();
        if (audio)
        {
            if (clips.Length > 0)
                audio.clip = clips[Random.Range(0, clips.Length)];
            audio.pitch *= Random.Range(1 - randomizePitch, 1 + randomizePitch);
            audio.Play();
        }
    }
}
