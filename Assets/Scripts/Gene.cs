﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Gene
{
    [Header("Auto set:")]
    public float Value;
    public int geneID = -1;
    private static int nextGeneID = 0;

    [Header("Parameters:")]
    public float defaultValue;
    public float min;
    public float max;
    [Range(0, 1)]
    public float mutationChance = 0.5f;
    [Range(0, 2)]
    public float mutationMultiplied = 0.2f;
    [Min(0)]
    public float mutationAdditive = 0;

    public void InitNewFromDefaults()
    {
        Value = defaultValue;
        geneID = nextGeneID++;
    }

    public void InitNewRandomized()
    {
        InitNewFromDefaults();
        for(int i = 0; i < 10; i++)
        {
            MutateValue();
        }
    }

    public void InitFromSoloParent(Gene baseGene)
    {
        Value = baseGene.Value;
        geneID = baseGene.geneID;
        defaultValue = baseGene.defaultValue;
        min = baseGene.min;
        max = baseGene.max;
        mutationChance = baseGene.mutationChance;
        mutationMultiplied = baseGene.mutationMultiplied;
        mutationAdditive = baseGene.mutationAdditive;

        MutateValue();
    }
    public void InitFromParents(Gene mother, Gene father)
    {
        var dominantParent = Random.value > 0.5 ? mother : father;

        Value = (mother.Value + father.Value + dominantParent.Value) / 3.0f;
        geneID = dominantParent.geneID;
        defaultValue = dominantParent.defaultValue;
        min = dominantParent.min;
        max = dominantParent.max;
        mutationChance = dominantParent.mutationChance;
        mutationMultiplied = dominantParent.mutationMultiplied;
        mutationAdditive = dominantParent.mutationAdditive;

        MutateValue();
    }

    public void MutateValue()
    {
        if (Random.value > mutationChance)
            return;

        if(Random.value < 0.5f)
            Value = Value / (1 + Random.Range(0, mutationMultiplied));
        else
            Value = Value * (1 + Random.Range(0, mutationMultiplied));

        if(mutationAdditive > 0)
            Value += Random.Range(-mutationAdditive, mutationAdditive);

        Value = Mathf.Clamp(Value, min, max);
    }
}
