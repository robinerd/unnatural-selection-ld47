﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGeneCollection
{
    void OnSpawned();
    void GetGenes(List<Gene> allGenes);
}
